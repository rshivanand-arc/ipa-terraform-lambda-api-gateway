#############################
# ACM Certificate
#############################
data "aws_acm_certificate" "cert" {
  domain = "${var.certificate_domain}"
}

#############################
# Route 53
#############################
resource "aws_route53_record" "api_record" {
  name    = "${var.domain_name}"
  type    = "A"
  zone_id = "${var.zone_id}"

  alias {
    evaluate_target_health = true
    name                   = "${aws_api_gateway_domain_name.api_domain.regional_domain_name}"
    zone_id                = "${aws_api_gateway_domain_name.api_domain.regional_zone_id}"
  }
}
