# CloudWatch Log Group
resource "aws_cloudwatch_log_group" "log_group" {
  name = "/aws/apigateway/${var.function_name}" # Naming log group same as lambda function for log group consistency

  tags = {
    Name         = "${var.lambda_role_name}"
    Environment  = "${var.tag_environment}"
    CreatorOwner = "${var.tag_creator_owner}"
    CostCenter   = "${var.tag_cost_center}"
    Scheduled    = "${var.tag_scheduled}"
    Product      = "${var.tag_product}"
    ProductCode  = "${var.tag_product_code}"
    Team         = "${var.tag_team}"
    Role         = "Cloudwatch Log Group"
    Application  = "${var.tag_application}"
    Deployment   = "${var.tag_deployment_method}"
    StorageTier  = "${var.tag_storage_tier}"
  }
}
