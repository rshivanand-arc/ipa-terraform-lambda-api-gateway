#############################
# Lambda
#############################

/*
The Content-Type of this file needs to be text/* or this will fail because Terraform won't pull down the body of the file.
*/
data "aws_s3_bucket_object" "lambda_code_bundle" {
  bucket = "${var.lambda_s3_bucket}"
  key    = "${var.lambda_s3_key}"
}

locals {
  default_environment_vars = {
    base_url = "${replace(var.domain_name, "/^[^.]+/", "")}"
    NODE_ENV = "production"
  }
}

resource "aws_lambda_function" "function" {
  function_name    = "${var.function_name}"
  s3_bucket        = "${var.lambda_s3_bucket}"
  s3_key           = "${var.lambda_s3_key}"
  source_code_hash = "${base64sha256("${data.aws_s3_bucket_object.lambda_code_bundle.key}")}"
  handler          = "${var.lambda_handler}"
  runtime          = "${var.lambda_runtime}"
  role             = "${aws_iam_role.lambda_role.arn}"
  layers           = ["${var.lambda_layer_arn}"]
  timeout          = 300
  memory_size      = 512

  environment = {
    variables = "${merge(var.lambda_environment_vars, local.default_environment_vars)}"
  }

  tracing_config {
    mode = "${var.tracing_mode}"
  }

  vpc_config {
    subnet_ids         = "${var.lambda_vpc_subnet_ids}"
    security_group_ids = "${var.lambda_vpc_security_group_ids}"
  }

  tags = {
    Name         = "${var.function_name}"
    Environment  = "${var.tag_environment}"
    CreatorOwner = "${var.tag_creator_owner}"
    CostCenter   = "${var.tag_cost_center}"
    Scheduled    = "${var.tag_scheduled}"
    Product      = "${var.tag_product}"
    ProductCode  = "${var.tag_product_code}"
    Team         = "${var.tag_team}"
    Role         = "Lambda"
    Application  = "${var.tag_application}"
    Deployment   = "${var.tag_deployment_method}"
    StorageTier  = "${var.tag_storage_tier}"
  }
}

resource "aws_lambda_permission" "apigw_permission" {
  statement_id  = "AllowAPIInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.function.function_name}"
  principal     = "apigateway.amazonaws.com"

  # The /*/*/* part allows invocation from any stage, method and resource path
  # within API Gateway REST API.
  source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*/*"
}

#############################
# Lambda Role
#############################
resource "aws_iam_role" "lambda_role" {
  name = "${var.lambda_role_name}"

  assume_role_policy = "${file("${var.assume_role_policy_file_path}")}"

  tags = {
    Name         = "${var.lambda_role_name}"
    Environment  = "${var.tag_environment}"
    CreatorOwner = "${var.tag_creator_owner}"
    CostCenter   = "${var.tag_cost_center}"
    Scheduled    = "${var.tag_scheduled}"
    Product      = "${var.tag_product}"
    ProductCode  = "${var.tag_product_code}"
    Team         = "${var.tag_team}"
    Role         = "IAM Role"
    Application  = "${var.tag_application}"
    Deployment   = "${var.tag_deployment_method}"
    StorageTier  = "${var.tag_storage_tier}"
  }
}

#############################
# Policies
#############################

resource "aws_iam_role_policy_attachment" "terraform_lambda_policy_attachment" {
  role       = "${aws_iam_role.lambda_role.id}"
  policy_arn = "${var.lambda_policy_arn}"
}
