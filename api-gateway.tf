#############################
# API Gateway
#############################
resource "aws_api_gateway_rest_api" "api" {
  name        = "${var.api_name}"
  description = "${var.api_description}"

  endpoint_configuration {
    types = ["REGIONAL"]
  }

  binary_media_types = [
    "${var.binary_media_types}"
  ]
}

# Deployment
resource "aws_api_gateway_deployment" "api_deployment" {
  depends_on = [
    "aws_api_gateway_integration.lambda_integration",
  ]
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  #stage_name  = "${var.stage_name}"
  variables {
    deployed_at = "${timestamp()}"
  }
  lifecycle {
    create_before_destroy = true
  }
}

# Resource
resource "aws_api_gateway_resource" "api_resource" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  parent_id   = "${aws_api_gateway_rest_api.api.root_resource_id}"
  path_part   = "${var.api_path_part}"
}

# Method
resource "aws_api_gateway_method" "api_method" {
  rest_api_id   = "${aws_api_gateway_rest_api.api.id}"
  resource_id   = "${aws_api_gateway_resource.api_resource.id}"
  http_method   = "${var.api_http_method}"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.authorizer.id}"
}

resource "aws_api_gateway_method_settings" "method_settings" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  stage_name  = "${aws_api_gateway_stage.stage.stage_name}"
  #method_path = "*/*"                                       # Enable for all methods
  method_path = "${var.method_path}"

  settings {
    metrics_enabled    = true
    data_trace_enabled = true
    logging_level      = "INFO"
  }
}

# Integration
resource "aws_api_gateway_integration" "lambda_integration" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  resource_id = "${aws_api_gateway_resource.api_resource.id}"
  http_method = "${aws_api_gateway_method.api_method.http_method}"

  request_templates    = "${var.request_templates}"
  passthrough_behavior = "WHEN_NO_MATCH"

  integration_http_method = "POST"
  type                    = "${var.integration_type}"
  uri                     = "${aws_lambda_function.function.invoke_arn}"
}

resource "aws_api_gateway_stage" "stage" {
  stage_name           = "${var.stage_name}"
  rest_api_id          = "${aws_api_gateway_rest_api.api.id}"
  deployment_id        = "${aws_api_gateway_deployment.api_deployment.id}"
  xray_tracing_enabled = "true"

  access_log_settings {
    destination_arn = "${aws_cloudwatch_log_group.log_group.arn}"
    format          = "${file("${path.module}/log_format.json")}"
  }

  tags = {
    Name         = "${var.lambda_role_name}"
    Environment  = "${var.tag_environment}"
    CreatorOwner = "${var.tag_creator_owner}"
    CostCenter   = "${var.tag_cost_center}"
    Scheduled    = "${var.tag_scheduled}"
    Product      = "${var.tag_product}"
    ProductCode  = "${var.tag_product_code}"
    Team         = "${var.tag_team}"
    Role         = "API Gateway Stage"
    Application  = "${var.tag_application}"
    Deployment   = "${var.tag_deployment_method}"
    StorageTier  = "${var.tag_storage_tier}"
  }
}

# Domain Name
resource "aws_api_gateway_domain_name" "api_domain" {
  regional_certificate_arn = "${data.aws_acm_certificate.cert.arn}"
  domain_name              = "${var.domain_name}"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

# Mapping
resource "aws_api_gateway_base_path_mapping" "mapping" {
  api_id      = "${aws_api_gateway_rest_api.api.id}"
  stage_name  = "${aws_api_gateway_stage.stage.stage_name}"
  domain_name = "${aws_api_gateway_domain_name.api_domain.domain_name}"
}
