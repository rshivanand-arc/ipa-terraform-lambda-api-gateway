terraform-lambda-api-gateway
===========

Terraform AWS API Gateway and Lambda module

#### Table of Contents

1. [Usage](#usage)
2. [Input Variables](#input-variables)
3. [Output Variables](#output-variables)
4. [Dependencies - Required external modules](#dependencies)

Usage
-----

Add to your terraform code.

```hcl
  module "api_gateway_lambda" {
    source = "git::ssh://git@bitbucket.org:arctravel/tip-terraform-lambda-api-gateway.git"

    # API Gateway
    api_name        = "${var.api_name}"
    api_description = "${var.api_description}"
    api_http_method = "${var.api_http_method}"  # ANY or GET or POST or etc...
    api_path_part =   "${var.api_path_part}"
    method_path   =   "${var.method_path}"
    integration_type =   "${var.integration_type}"  # defaulted to AWS_PROXY if not passed
    request_templates =   "${var.request_templates}" # defaulted to empty map if not passed
    options_request_templates =   "${var.options_request_templates}" # defaulted to "{"application/json" = "{ 'statusCode' : 200 }"}" if not passed
    options_response_models =   "${var.options_response_models}"  # defaulted to "{"application/json" = "Empty"}" if not passed
    stage_name      = "${var.stage_name}"
    binary_media_types      = "${var.binary_media_types}"

    # Authorizer
    authorizer_name        = "${var.authorizer_name}"
    authorizer_uri   = "${var.authorizer_uri}"
    invocation_role_name = "${var.invocation_role_name}"
    invocation_policy_name = "${var.invocation_policy_name}"
    authorizer_lambda_arn   = "${var.authorizer_lambda_arn}"
    auth_ttl               = "${var.auth_ttl}" # defaulted to 300 if not passed

    # Route 53
    certificate_domain = "${var.certificate_domain}"
    domain_name        = "${var.domain_name}"
    zone_id            = "${var.zone_id}"

    # Lambda
    function_name     = "${var.function_name}"
    lambda_s3_bucket  = "${var.lambda_s3_bucket}"
    lambda_s3_key     = "${var.lambda_s3_key}"
    lambda_handler    = "${var.lambda_handler}"
    lambda_role_name  = "${var.lambda_role_name}"
    lambda_policy_arn = "${var.lambda_policy_arn}"
    lambda_layer_arn = "${var.lambda_layer_arn}"
    lambda_runtime    = "${var.lambda_runtime}"  # nodejs10.x or python3.7 or etc..
    lambda_environment_vars = "${var.lambda_environment_vars}" # defaulted to empty map if not passed
    assume_role_policy_file_path = "${var.assume_role_policy_file_path}" # defaulted to "assume_role_policy.json" if not passed
    lambda_vpc_subnet_ids         = "${var.lambda_vpc_subnet_ids}"
    lambda_vpc_security_group_ids = "${var.lambda_vpc_security_group_ids}"

  }
```

Input Variables
----------------------

This module supports the following input variables.

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|----------|
| api_description | The description of the API Gateway. | string | none | yes  |
| api_name | The name of the API Gateway. | string | none | yes |
| auth_lambda_handler | The auth function entrypoint in your code. | string | none | yes |
| auth_lambda_name | The name of the auth Lambda function. | string | none | yes |
| auth_lambda_policy_arn | The ARN of the policy to attach to the auth Lambda role. | string | none | yes |
| auth_lambda_role_name | The name of the role to attach to the auth Lambda. | string | none | yes |
| auth_lambda_s3_bucket | The S3 bucket location containing the auth lambda's deployment package. | string | none | yes |
| auth_lambda_s3_key | The S3 key of an object containing the auth lambda's deployment package. | string | none | yes |
| authorizer_name | The name of the authorizer. | string | none | yes |
| auth_ttl | The TTL of cached authorizer results in seconds | string | 300 | no |
| bucket | Name of the S3 bucket where state will be stored. | string | none | yes |
| certificate_domain | The domain for the certificate you wish to used. | string | none | yes |
| domain_name | The fully-qualified domain name to register. | string | none | yes |
| dyanamodb_table | The name of the DynamoDB table used to lock the state. | string | none | yes |
| function_name | The name of the Lambda function. | string | none | yes |
| invocation_policy_name | The name of the policy to attach to the invocation role. | string | none | yes |
| invocation_role_name | The name of the role to attach to the authorizer. | string | none | yes |
| key | The key used for the state file. | string | none | yes |
| lambda_handler | The function entrypoint in your code. | string | none | yes |
| lambda_policy_arn | The ARN of the policy to attach to the Lambda role. | string | none | yes |
| lambda_role_name | The name of the IAM role to attach to the Lambda. | string | none | yes |
| lambda_s3_bucket | The S3 bucket location containing the function's deployment package. | string | none | yes |
| lambda_s3_key | The S3 key of an object containing the function's deployment package. | string | none | yes |
| region | The name of the AWS region to run in. | string | none | yes |
| stage_name | The name of the deployment stage. | string | none | yes |
| zone_id | The domain for the certificate you wish to use | string | none | yes |

Output Variables
-----

This module supports the following output variables.

| Name | Description | Type |
|------|-------------|------|
| invoke_url | The invoke URL of the API | string |
| base_url | The friendly DNS base URL of the API | string |
| lambda_role_arn | Lambda role's arn | string |
| lambda_source_code_hash | Hash of lambda source code bundle | string |

Dependencies
-----

N/A
