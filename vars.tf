#############################
# API Gateway
#############################
variable "api_name" {
  description = "The name of the API Gateway"
  type        = "string"
}

variable "api_description" {
  description = "The description of the API Gateway"
  type        = "string"
}

variable "api_http_method" {
  description = "The http method of the API"
  type        = "string"
}

variable "api_path_part" {
  description = "The path part the API"
  type        = "string"
  default     = "{proxy+}"
}

variable "method_path" {
  description = "The method path of the API"
  type        = "string"
}

variable "integration_type" {
  description = "The integration method"
  type        = "string"
  default     = "AWS_PROXY"
}

variable "request_templates" {
  description = "The integration request_templates"
  type        = "map"
  default     = {}
}

variable "options_request_templates" {
  description = "The integration options_request_templates"
  type        = "map"

  default = {
    "application/json" = "{ 'statusCode' : 200 }"
  }
}

variable "options_response_models" {
  description = "The method options_response_models"
  type        = "map"

  default = {
    "application/json" = "Empty"
  }
}

variable "stage_name" {
  description = "The name of the deployment stage"
  type        = "string"
}

variable "binary_media_types" {
  description = "Binary media types allowed in response"
  type        = "string"
  default     = "UTF-8"
}

#############################
# Authorizer
#############################
variable "authorizer_name" {
  description = "The name of the authorizer"
  type        = "string"
}

variable "authorizer_uri" {
  description = "The name of the authorizer"
  type        = "string"
  default     = "arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/arn:aws:lambda:us-east-1:082150747706:function:tip-node-lambda-api-authorizer/invocations"
}

variable "invocation_role_name" {
  description = "The name of the role to attach to the authorizer"
  type        = "string"
}

variable "invocation_policy_name" {
  description = "The policy to attach to the invocation role"
  type        = "string"
}

variable "authorizer_lambda_arn" {
  description = "The arn of the auth Lambda function"
  type        = "string"
  default     = "arn:aws:lambda:us-east-1:082150747706:function:tip-node-lambda-api-authorizer"
}

variable "auth_ttl" {
  description = "The TTL of cached authorizer results in seconds."
  type        = "string"
  default     = "300"
}

variable "auth_lambda_environment_vars" {
  description = "NodeJS environment variables map to pass into our Authorizer Lambda"
  type        = "map"
  default     = {}
}

#########################

# Route 53
#############################
variable "domain_name" {
  description = "The fully-qualified domain name to register"
  type        = "string"
}

variable "zone_id" {
  description = "The Hosted Zone ID of the desired Hosted Zone"
  type        = "string"
}

variable "certificate_domain" {
  description = "The domain for the certificate you wish to use"
  type        = "string"
}

#############################
# Lambda
#############################
variable "function_name" {
  description = "The name of the Lambda function"
  type        = "string"
}

variable "lambda_s3_bucket" {
  description = "The S3 bucket location containing the function's deployment package"
  type        = "string"
}

variable "lambda_s3_key" {
  description = "The S3 key of an object containing the function's deployment package"
  type        = "string"
}

variable "lambda_handler" {
  description = "The function entrypoint in your code"
  type        = "string"
}

variable "lambda_runtime" {
  description = "The runtime environment for lambda function"
  type        = "string"
}

variable "lambda_role_name" {
  description = "The name of the IAM role to attach to the Lambda"
  type        = "string"
}

variable "lambda_policy_arn" {
  description = "The ARN of the policy to attach to the Lambda role"
  type        = "string"
}

variable "lambda_layer_arn" {
  description = "The ARN of the layer to attach to the Lambda function"
  type        = "string"
  default     = ""
}

variable "lambda_vpc_subnet_ids" {
  description = "vpc subnet ids where Lambda needs to be executed"
  type        = "list"
  default     = [""]
}

variable "lambda_vpc_security_group_ids" {
  description = "vpc subnet ids where Lambda needs to be executed"
  type        = "list"
  default     = [""]
}

variable "lambda_environment_vars" {
  description = "NodeJS environment variables map to pass into our Lambda"
  type        = "map"
  default     = {}
}

variable "assume_role_policy_file_path" {
  description = "File path where assume role policy is defined"
  type        = "string"
  default     = "assume_role_policy.json"
}

variable "tracing_mode" {
  description = "How the Lambda should handle tracing"
  type        = "string"
  default     = "Active"
}

###############################################################################################
# Tags ref: https://arccorp.atlassian.net/wiki/spaces/IS/pages/268538188/AWS+Tagging+Values
###############################################################################################
variable "tag_environment" {
  description = "The application environment i.e. dev, int, nonprod, qa, prod, uat"
  type        = "string"
  default     = "dev"
}

variable "tag_creator_owner" {
  description = "Name of the owner of the resource."
  type        = "string"
  default     = "Webapps Team"
}

variable "tag_cost_center" {
  description = "Cost Center"
  type        = "string"
  default     = "646"
}

variable "tag_scheduled" {
  description = "Only set this if the infrastructure needs to be shut down at certain times."
  type        = "string"
  default     = "NA"
}

variable "tag_product" {
  description = "Product name this resource belongs to."
  type        = "string"
  default     = "TIP"
}

variable "tag_product_code" {
  description = "Product code."
  type        = "string"
  default     = "45090"
}

variable "tag_team" {
  description = "Team name"
  type        = "string"
  default     = "Webapps"
}

variable "tag_application" {
  description = "Applicaiton name."
  type        = "string"
  default     = "TIP_MONITORING"
}

variable "tag_deployment_method" {
  description = "Cloudformation, Terraform or manual."
  type        = "string"
  default     = "Terraform"
}

variable "tag_storage_tier" {
  description = "S3, Glacier, EBS, EFS"
  type        = "string"
  default     = "NA"
}
