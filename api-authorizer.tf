#############################
# API Gateway Authorizer
#############################
resource "aws_api_gateway_authorizer" "authorizer" {
  name                             = "${var.authorizer_name}"
  rest_api_id                      = "${aws_api_gateway_rest_api.api.id}"
  authorizer_uri                   = "${var.authorizer_uri}"
  authorizer_credentials           = "${aws_iam_role.invocation_role.arn}"
  authorizer_result_ttl_in_seconds = "${var.auth_ttl}"
}

# Assume Role Policy for Authorizer Role
data "aws_iam_policy_document" "invocation_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}

# Authorizer Role
resource "aws_iam_role" "invocation_role" {
  name = "${var.invocation_role_name}"
  path = "/"

  assume_role_policy = "${data.aws_iam_policy_document.invocation_assume_role_policy.json}"

  tags = {
    Name         = "${var.function_name}"
    Environment  = "${var.tag_environment}"
    CreatorOwner = "${var.tag_creator_owner}"
    CostCenter   = "${var.tag_cost_center}"
    Scheduled    = "${var.tag_scheduled}"
    Product      = "${var.tag_product}"
    ProductCode  = "${var.tag_product_code}"
    Team         = "${var.tag_team}"
    Role         = "IAM Role"
    Application  = "${var.tag_application}"
    Deployment   = "${var.tag_deployment_method}"
    StorageTier  = "${var.tag_storage_tier}"
  }
}

# Policy for Authorizer Role
data "aws_iam_policy_document" "invocation_policy" {
  statement {
    actions = ["lambda:InvokeFunction"]

    resources = ["${var.authorizer_lambda_arn}"]

    effect = "Allow"
  }
}

# Policy Attachment for Authorizer Role
resource "aws_iam_role_policy" "invocation_policy" {
  name = "${var.invocation_policy_name}"
  role = "${aws_iam_role.invocation_role.id}"

  policy = "${data.aws_iam_policy_document.invocation_policy.json}"
}
