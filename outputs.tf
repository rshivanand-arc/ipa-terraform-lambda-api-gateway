 output "invoke_url" {
  value = "${aws_api_gateway_deployment.api_deployment.invoke_url}"
}
 
output "lambda_role_arn" {
  value = "${aws_iam_role.lambda_role.arn}"
}

output "lambda_source_code_hash" {
  value = "${aws_lambda_function.function.source_code_hash}"
}

output "base_url" {
  value = "${replace(var.domain_name, "/^[^.]+/", "")}"
}

output "rest_api_id" {
  value = "${aws_api_gateway_rest_api.api.id}"
}

output "api_resource_id" {
  value = "${aws_api_gateway_resource.api_resource.id}"
}
